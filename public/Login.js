//getElementById, referencia a un elemento por su valor de atributo id
const firebaseConfig = {
  apiKey: "AIzaSyDK0PV6Qf3SW-518nyUWZCoReQR_wSjOsw",
  authDomain: "periodo-de-recuperacion.firebaseapp.com",
  databaseURL: "https://periodo-de-recuperacion-default-rtdb.firebaseio.com",
  projectId: "periodo-de-recuperacion",
  storageBucket: "periodo-de-recuperacion.appspot.com",
  messagingSenderId: "787910666382",
  appId: "1:787910666382:web:e46106505a57611847c1df"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig); // Metodo para inicializar Firebase con la información del objeto

var boton=document.getElementById("ingresarId"); 
boton.addEventListener("click",ingresar,false) // Añado un "listening" al elemento. Cuando exista el click, tiene que ejecutar algo

function ingresar(){
    var email=document.getElementById("email").value; // Devuelve el valor del elemento con id "email"
    var password=document.getElementById("password").value;

  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then(()=>{
    return firebase.auth().signInWithEmailAndPassword(email, password) // Función de firebase para iniciar sesion con la información
    .then((userCredential) => { //Se ejecuta, si la información en el es correcta
      // Signed in
      console.log("Entraste")
      window.location= ("Medicion.html");
      // ...
    })
    .catch((error) => { //Se ejecuta, si la información en el es incorrecta
      alert("Verifique sus credenciales")
    });
  })


}

var boton=document.getElementById("registrarseId"); 
boton.addEventListener("click",registrar,false) 
var modal=bootstrap.Modal.getOrCreateInstance(document.getElementById("miModal"));

function registrar(){
  var email=document.getElementById("emailmodal").value;
  var password=document.getElementById("passwordmodal").value;

  firebase.auth().createUserWithEmailAndPassword(email, password)
  .then((userCredential) => {
    // Signed in
    var user = userCredential.user;
    modal.hide();
    alert("Se ha registrado correctamente, ya puede probar ingresar al sistema");
    // ...
  })
  .catch((error) => {
    var errorCode = error.code;
    var errorMessage = error.message;
    alert("Verifique su email o que la contraseña sea mayor a 6 caracteres");
    // ..
  });
}

