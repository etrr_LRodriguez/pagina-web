//Temperatura - Firebase

const firebaseConfig = {
    apiKey: "AIzaSyDK0PV6Qf3SW-518nyUWZCoReQR_wSjOsw",
    authDomain: "periodo-de-recuperacion.firebaseapp.com",
    databaseURL: "https://periodo-de-recuperacion-default-rtdb.firebaseio.com",
    projectId: "periodo-de-recuperacion",
    storageBucket: "periodo-de-recuperacion.appspot.com",
    messagingSenderId: "787910666382",
    appId: "1:787910666382:web:e46106505a57611847c1df"
  };

firebase.initializeApp(firebaseConfig);

var temperatura = document.getElementById("temperatura_real");

var funcion1 = false; 
firebase.database().ref('/temperatura_real').on('value', (snapshot) => { //Hace referencia a la ruta en la cual voy a obtener el valor. on es un metodo de firebase para leer
  data = snapshot.val();
  temperatura.innerHTML = snapshot.val() + "°C";
});

firebase.auth().onAuthStateChanged((user) => { //Si el usuario se encuentra en la pagina de medición y no esta logeado, sera automaticamente redirigido a la pagina de Login
  if (!user) {
   window.location="Login.html"
  } 
});
