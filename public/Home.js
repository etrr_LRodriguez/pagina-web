const firebaseConfig = {
    apiKey: "AIzaSyDK0PV6Qf3SW-518nyUWZCoReQR_wSjOsw",
    authDomain: "periodo-de-recuperacion.firebaseapp.com",
    databaseURL: "https://periodo-de-recuperacion-default-rtdb.firebaseio.com",
    projectId: "periodo-de-recuperacion",
    storageBucket: "periodo-de-recuperacion.appspot.com",
    messagingSenderId: "787910666382",
    appId: "1:787910666382:web:e46106505a57611847c1df"
  };

firebase.initializeApp(firebaseConfig);

firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      // User is signed in, see docs for a list of available properties
      // https://firebase.google.com/docs/reference/js/firebase.User
      var uid = user.uid;
      console.log(uid)
      var boton = document.getElementById("boton_login")
      boton.addEventListener("click",logout,false)
      boton.className="btn btn-danger"
      boton.innerHTML="Log out"
      document.getElementById("boton_temp").style.display="block"
    } 
    
    else {
      // User is signed out
      
      console.log("nope")
    }
  });

  function logout(){
    firebase.auth().signOut();
  }